import Vue from 'vue';
import en from 'vee-validate/dist/locale/en';
import VeeValidate, { Validator } from 'vee-validate';

Vue.use(VeeValidate, {
    inject: true,
    fieldsBagName: 'veeFields',
    errorBagName: 'veeErrors',
});
Validator.localize('en', en);
