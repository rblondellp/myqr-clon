import Vue from 'vue'; // in Vue 2
import axios from 'axios';
import VueAxios from 'vue-axios';
import url from './api';

Vue.use(VueAxios, axios);
Vue.axios.defaults.baseURL = url;
Vue.axios.defaults.headers.common['Authorization'] = window.localStorage.token;

const ApiService = {
    get(resource, slug = '') {
        return Vue.axios.get(
            !slug ? `api/${resource}` : `api/${resource}/${slug}`
        );
    },

    post(resource, params) {
        return Vue.axios.post(`api/${resource}`, params);
    },

    update(module, id, params) {
        return Vue.axios.put(`api/${module}/${id}`, params);
    },

    del(module, id) {
        return Vue.axios.delete(`api/${module}/${id}`);
    },
};

export default ApiService;
