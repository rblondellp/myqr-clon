import Vue from "vue";
import VueRouter from "vue-router";
import PublicLayout from "../views/layouts/PublicLayout";
import PublicLayout2 from "../views/layouts/PublicLayout2";
import AdminLayout from "../views/layouts/admin/AdminLayout";
import Main from "../views/public/main/Main";
import Conditions from "../views/public/conditions/Conditions";
import PrivacyPolicy from "../views/public/privacy_policy/PrivacyPolicy";
import CookiesPolicy from "../views/public/cookies_policy/CookiesPolicy";
import QrCodes from "../views/admin/qr_codes/QrCodes";
import ContactForm from "../views/public/contact_form/ContactForm";
import Prices from "../views/public/prices/Prices";

Vue.use(VueRouter);

const routes = [
    {
        path: "/",
        name: "public",
        component: PublicLayout,
        children: [
            {
                path: "/",
                name: "main",
                component: Main,
            },
        ],
    },
    {
        path: "/",
        name: "public_layout_2",
        component: PublicLayout2,
        children: [
            {
                path: "/conditions-of-use-and-contract",
                name: "conditions",
                component: Conditions,
            },
            {
                path: "/privacy-policy",
                name: "privacy_policy",
                component: PrivacyPolicy,
            },
            {
                path: "/cookie-policy",
                name: "cookie_policy",
                component: CookiesPolicy,
            },
            {
                path: "/contact-us",
                name: "contact_us",
                component: ContactForm,
            },
            {
                path: "/prices",
                name: "prices",
                component: Prices,
            },
        ],
    },
    {
        path: "/",
        name: "admin_layout",
        component: AdminLayout,
        children: [
            {
                path: "/my-qr-codes",
                name: "qr_codes",
                component: QrCodes,
            },
        ],
        meta: { auth: true },
    },
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes,
});

export default router;
