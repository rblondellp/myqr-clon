<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Password;
use Auth;
use DB;
use App\Models\User;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function login(Request $request) {
        try {
            $credentials = $request->validate([
                'email' => ['required', 'email'],
                'password' => ['required'],
            ]);

            if (!Auth::attempt($credentials)) return response()->json('Usuario o contraseña inválida', 400);

            $token = $request->user()->createToken('Personal Access Token')->plainTextToken;

            return response()->json([
                'usuario' => $request->user(),
                'access_token' => $token,
                'token_type' => 'Bearer'
            ], 200);
        } catch (\Exception $e) {
            return response()->json('Error - Login ' . $e, 500);
        }
    }

    public function register(Request $request) {
        try {
            $request->validate([
                'email' => ['required', 'email'],
                'password' => ['required'],
            ]);
            
            DB::beginTransaction();

            if(User::where('email', $request->email)->exists()) return response()->json('El email ingresado no está disponible', 400);

            $newUser = new User;
            $newUser->name = '';
            $newUser->email = $request->email;
            $newUser->password = bcrypt($request->password);
            $newUser->save();

            DB::commit();
            return $this->login($request);
            // return response()->json('Usuario registrado con éxito', 201);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json('Error - Register ' . $e, 500);
        }
    }

    public function logout(Request $request)
    {
        $request->user()->tokens()->delete();
        return response()->json('¡Sección cerrada con éxito!', 200);
    }

    public function getUser() {
        return response()->json(Auth::user(), 200);
    }

    public function resetPassword(Request $request){
        try {
            DB::beginTransaction();

            $request->validate(['email' => 'required|email']);

            $user = User::where('email', $request->email)->first();
            if(!$user) return response()->json('User does not exist', 404);

            $token = Str::random(50);
            DB::table('password_resets')->insert([
                'email' => $request->email,
                'token' => $token,
                'created_at' => Carbon::now()
            ]);

            DB::commit();
            return response()->json('A reset link has been sent to your email address.', 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json('Error - resetPassword: ' . $e, 500);
        }
        // return redirect()->back()->with('status', trans('A reset link has been sent to your email address.'));

        // if ($this->sendResetEmail($request->email, $tokenData->token)) {
            
        // } else {
        //     return redirect()->back()->withErrors(['error' => trans('A Network Error occurred. Please try again.')]);
        // }
    }

    private function sendResetEmail($email, $token){
        $user = User::where('email', $email)->select('email')->first();
        $link = config('base_url') . 'password/reset/' . $token . '?email=' . urlencode($user->email);

        try {
        //Here send the link with CURL with an external email API 
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function resetPasswordUser(Request $request){
        try {
            DB::beginTransaction();

            $validator = Validator::make($request->all(), [
                'email' => 'required|email|exists:users,email',
                'password' => 'required|confirmed',
                'token' => 'required' 
            ]);
    
            if ($validator->fails()) return response()->json('Please complete the form', 400);
    
            $password = $request->password;
            $tokenData = DB::table('password_resets')->where('token', $request->token)->first();
            if (!$tokenData) return response()->json('The token is invalid', 400);
    
            $user = User::where('email', $tokenData->email)->first();
            if (!$user) return response()->json('Email not found', 404);
        
            $user->password = bcrypt($password);
            $user->save();
    
            // //login the user immediately they change password successfully
            // Auth::login($user);
    
            //Delete the token
            DB::table('password_resets')->where('email', $user->email)->delete();
    
            // //Send Email Reset Success Email
            // if ($this->sendSuccessEmail($tokenData->email)) {
            //     return view('index');
            // } else {
            //     return redirect()->back()->withErrors(['email' => trans('A Network Error occurred. Please try again.')]);
            // }

            DB::commit();
            return response()->json('Password reset successful', 200);
        } catch (\Exception $e) {
            DB::rolback();
            return response()->json('Error - resetPasswordUser: ' . $e, 500);
        }
    }
}
