<?php

namespace App\Http\Controllers\ContactForm;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContactFormController extends Controller
{
    public function contactForm(Request $request) {
        try {
            $request->validate([
                'name' => ['required'],
                'email' => ['required', 'email'],
                'subject' => ['required', 'alpha'],
                'question' => ['required']
            ]);

            $name = $request->name;
            $email = $request->email;
            $subject = $request->subject;
            $question = $request->question;

            return response()->json('Mail send successful', 200);
        } catch (\Exception $e) {
            return response()->json($e, 500);
        }
    }
}
